# godot-justfile

Justfile for Godot game development on Linux from the command line I have made for myself.
Note that this justfile may be incomplete, and can be buggy, but I use it regularly with relative ease.
I decided to share it in case someone finds it useful.

## Requirements

* [just](https://github.com/casey/just)
    * Note: Installation with Cargo is recommended to get the latest version.
* The [requirements to compile Godot](https://docs.godotengine.org/en/stable/development/compiling/compiling_for_x11.html) and the [source code](https://github.com/godotengine/godot/releases) of the engine
* Optionally [fzf fuzzy finder](https://github.com/junegunn/fzf)

## Setup

To use the justfile, the engine directory with the source code of the engine, and the project directory which contains the project.godot file need to be specified in the .env file.
The tasks take care of generating any neccessary binaries needed to run that specific task, so you can use the eg. `just run scenes/example_scene` task without first running the task to compile the engine binary.

## Usage

To list the available tasks, run `just -l`.
If you have fzf fuzzy finder installed, you can *just* type `just` and then select the desired task.

The most important tasks are:
- `just editor` or `just e`: runs the Godot editor
- `just run <scene>` or `just r <scene>`: runs the game with the specified scene file (without the .tscn extension). 

## Directory structure

On the first run, the justfile will create the `build`, `engine-binaries`, and `logs` directories. The following is an example tree of a working directory:

```
├── build (contians the exported game binaries)
├── engine (godot engine source)
│   ├── ...
│   └── version.py
├── engine-binaries
│   └── 4.0.0.alpha (name comes from version.py's contents)
│       └── editor_data
│           ├── ...
│           └── templates (stores the export templates)
├── project (game project's dir)
│   ├── ...
│   ├── scenes
│   │   └── example_scene.tscn
│   └── project.godot
└── logs (logs for the latest debug run)
```